<?php
namespace oteixido\importer;

interface ImporterInterface
{
    public function importFromFile($filename, $save = true);
}
