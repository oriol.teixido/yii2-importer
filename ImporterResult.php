<?php
namespace oteixido\importer;

class ImporterResult
{
    private $_errors = [];
    private $_models = [];

    public function isSuccess()
    {
        return count($this->_errors) == 0;
    }

    public function count()
    {
        return count($this->_models);
    }

    public function addError($line, $error)
    {
        if (!array_key_exists($line, $this->_errors)) {
            $this->_errors[$line] = [];
        }
        $this->_errors[$line][] = $error;
    }

    public function addModel($line, $model)
    {
        $this->_models[$line] = $model;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function getModels()
    {
        return $this->_models;
    }
}
