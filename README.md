Yii2 importer
=============
Yii2 importer

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist oteixido/yii2-importer "*"
```

or add

```
"oteixido/yii2-importer": "*"
```

to the require section of your `composer.json` file.
