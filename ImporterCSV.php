<?php
namespace oteixido\importer;

use yii\base\Component;
use yii\i18n\PhpMessageSource;
use Yii;

class ImporterCSV extends Component implements ImporterInterface
{
    public $classname;
    public $attributes;
    public $separator = ';';
    public $maxLength = 1000;

    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/importer'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-importer/messages'
        ];
    }

    public function importFromFile($filename, $save = true)
    {
        $result = $this->loadCSV($filename);
        if (!$result->isSuccess()) {
            return $result;
        }
        $result = $this->validateModels($result);
        if (!$result->isSuccess()) {
            return $result;
        }
        if ($save) {
            $result = $this->saveModels($result);
        }
        return $result;
    }

    private function loadCSV($filename)
    {
        $result = new ImporterResult();
        $line = 0;
        $handle = fopen($filename, 'r');
        while (($values = fgetcsv($handle, $this->maxLength, $this->separator)) !== false) {
            $line++;
            if (count($values) != count($this->attributes)) {
                $result->addError($line, Yii::t('oteixido/importer', 'El número de camps no és correcte.'));
                continue;
            }
            $result->addModel($line, $this->loadCSVLine($line, $values));
        }
        fclose($handle);
        return $result;
    }

    private function validateModels($result)
    {
        foreach ($result->getModels() as $line => $model) {
            if (!$model->validate()) {
                foreach ($model->getErrorSummary(true) as $error) {
                    $result->addError($line, $error);
                }
            }
        }
        return $result;
    }

    private function saveModels($result)
    {
        foreach ($result->getModels() as $line => $model) {
            if (!$model->save()) {
                $result->addError($line, Yii::t('oteixido/importer', 'No es pot importar l\'element.'));
            }
        }
        return $result;
    }

    private function loadCSVLine($line, $values)
    {
        $classname = $this->classname;
        $model = new $classname();
        $position = 0;
        foreach($this->attributes as $attribute) {
            $value = $values[$position];
            $name = $attribute['attribute'];
            $model->$name = $this->getAttributeValue($attribute, $value);
            $position++;
        }
        return $model;
    }

    private function getAttributeValue($attribute,  $value) {
        if (!empty($attribute['foreignClassname'])) {
            $foreignClassname = $attribute['foreignClassname'];
            $foreignAttribute = $attribute['foreignAttribute'];
            $foreignKey = $attribute['foreignKey'];
            $related = $foreignClassname::findOne([$foreignAttribute => $value]);
            if ($related != null) {
                $value = $related->$foreignKey;
            }
        }
        $filters = isset($attribute['filters']) ? $attribute['filters'] : [];
        foreach ($filters as $pattern => $replacement) {
            $value = preg_replace($pattern, $replacement, $value);
        }
        return $value;
    }
}
